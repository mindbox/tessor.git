#![allow(dead_code)]
use std::{iter::Cloned, slice::Iter};

use crate::{
    geom::{Point, Transform, Transformation},
    // geom::{cubic::CubicSegment, quadratic::QuadraticSegment, Point, Transform, Transformation},
    iter::{
        ExtendFromInternalIterator, FromInternalIterator, InternalIterator, IntoInternalIterator,
    },
    // line::LinePathCommand,
};

use cubic::CubicSegment;
use quadratic::QuadraticSegment;

/// A sequence of commands that defines a set of contours, each of which consists of a sequence of
/// curve segments. Each contour is either open or closed.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Path {
    verbs: Vec<Verb>,
    points: Vec<Point>,
}

impl Path {
    /// Creates a new empty path.
    pub fn new() -> Path {
        Path::default()
    }

    /// Returns a slice of the points that make up `self`.
    pub fn points(&self) -> &[Point] {
        &self.points
    }

    /// Returns an iterator over the commands that make up `self`.
    pub fn commands(&self) -> Commands {
        Commands {
            verbs: self.verbs.iter().cloned(),
            points: self.points.iter().cloned(),
        }
    }

    /// Returns a mutable slice of the points that make up `self`.
    pub fn points_mut(&mut self) -> &mut [Point] {
        &mut self.points
    }

    /// Adds a new contour, starting at the given point.
    pub fn move_to(&mut self, p: Point) {
        self.verbs.push(Verb::MoveTo);
        self.points.push(p);
    }

    /// Adds a line segment to the current contour, starting at the current point.
    pub fn line_to(&mut self, p: Point) {
        self.verbs.push(Verb::LineTo);
        self.points.push(p);
    }

    // Adds a quadratic Bezier curve segment to the current contour, starting at the current point.
    pub fn quadratic_to(&mut self, p1: Point, p: Point) {
        self.verbs.push(Verb::QuadraticTo);
        self.points.push(p1);
        self.points.push(p);
    }

    // Adds a quadratic Bezier curve segment to the current contour, starting at the current point.
    pub fn cubic_to(&mut self, p1: Point, p2: Point, p: Point) {
        self.verbs.push(Verb::CubicTo);
        self.points.push(p1);
        self.points.push(p2);
        self.points.push(p);
    }

    /// Closes the current contour.
    pub fn close(&mut self) {
        self.verbs.push(Verb::Close);
    }

    /// Clears `self`.
    pub fn clear(&mut self) {
        self.verbs.clear();
        self.points.clear();
    }
}

impl ExtendFromInternalIterator<PathCommand> for Path {
    fn extend_from_internal_iter<I>(&mut self, internal_iter: I)
    where
        I: IntoInternalIterator<Item = PathCommand>,
    {
        internal_iter.into_internal_iter().for_each(&mut |command| {
            match command {
                PathCommand::MoveTo(p) => self.move_to(p),
                PathCommand::LineTo(p) => self.line_to(p),
                PathCommand::QuadraticTo(p1, p) => self.quadratic_to(p1, p),
                PathCommand::CubicTo(p1, p2, p) => self.cubic_to(p1, p2, p),
                PathCommand::Close => self.close(),
            }
            true
        });
    }
}

impl FromInternalIterator<PathCommand> for Path {
    fn from_internal_iter<I>(internal_iter: I) -> Self
    where
        I: IntoInternalIterator<Item = PathCommand>,
    {
        let mut path = Path::new();
        path.extend_from_internal_iter(internal_iter);
        path
    }
}

impl Transform for Path {
    fn transform<T>(mut self, t: &T) -> Path
    where
        T: Transformation,
    {
        self.transform_mut(t);
        self
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        for point in self.points_mut() {
            point.transform_mut(t);
        }
    }
}

/// An iterator over the commands that make up a path.
#[derive(Clone, Debug)]
pub struct Commands<'a> {
    verbs: Cloned<Iter<'a, Verb>>,
    points: Cloned<Iter<'a, Point>>,
}

impl<'a> Iterator for Commands<'a> {
    type Item = PathCommand;

    fn next(&mut self) -> Option<PathCommand> {
        self.verbs.next().map(|verb| match verb {
            Verb::MoveTo => PathCommand::MoveTo(self.points.next().unwrap()),
            Verb::LineTo => PathCommand::LineTo(self.points.next().unwrap()),
            Verb::QuadraticTo => {
                PathCommand::QuadraticTo(self.points.next().unwrap(), self.points.next().unwrap())
            }
            Verb::CubicTo => PathCommand::CubicTo(
                self.points.next().unwrap(),
                self.points.next().unwrap(),
                self.points.next().unwrap(),
            ),
            Verb::Close => PathCommand::Close,
        })
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
enum Verb {
    MoveTo,
    LineTo,
    QuadraticTo,
    CubicTo,
    Close,
}

/// An extension trait for iterators over path commands.
pub trait PathIterator: InternalIterator<Item = PathCommand> {
    /// Returns an iterator over line path commands that approximate `self` with tolerance
    /// `epsilon`.
    fn linearize(self, epsilon: f32) -> Linearize<Self>
    where
        Self: Sized,
    {
        Linearize {
            path: self,
            epsilon,
        }
    }
}

impl<I> PathIterator for I where I: InternalIterator<Item = PathCommand> {}

/// An iterator over line path commands that approximate `self` with tolerance `epsilon`.
#[derive(Clone, Debug)]
pub struct Linearize<P> {
    path: P,
    epsilon: f32,
}

impl<P> Linearize<P>
where
    P: PathIterator,
{
    pub fn finish(self) -> Vec<LinePathCommand> {
        self.collect::<Vec<_>>()
    }
}

impl<P> InternalIterator for Linearize<P>
where
    P: PathIterator,
{
    type Item = LinePathCommand;

    fn for_each(self, f: &mut impl FnMut(Self::Item) -> bool) -> bool {
        let mut initial_point = None;
        let mut current_point = None;
        self.path.for_each({
            let epsilon = self.epsilon;
            &mut move |command| match command {
                PathCommand::MoveTo(p) => {
                    initial_point = Some(p);
                    current_point = Some(p);
                    f(LinePathCommand::MoveTo(p))
                }
                PathCommand::LineTo(p) => {
                    current_point = Some(p);
                    f(LinePathCommand::LineTo(p))
                }
                PathCommand::QuadraticTo(p1, p) => {
                    QuadraticSegment::new(current_point.unwrap(), p1, p)
                        .linearize(epsilon)
                        .for_each(&mut |p| {
                            current_point = Some(p);
                            f(LinePathCommand::LineTo(p))
                        })
                }
                PathCommand::CubicTo(p1, p2, p) => {
                    CubicSegment::new(current_point.unwrap(), p1, p2, p)
                        .linearize(epsilon)
                        .for_each(&mut |p| {
                            current_point = Some(p);
                            f(LinePathCommand::LineTo(p))
                        })
                }
                PathCommand::Close => {
                    current_point = initial_point;
                    f(LinePathCommand::Close)
                }
            }
        })
    }
}

/// A command in a path
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PathCommand {
    MoveTo(Point),
    LineTo(Point),
    QuadraticTo(Point, Point),
    CubicTo(Point, Point, Point),
    Close,
}

impl Transform for PathCommand {
    fn transform<T>(self, t: &T) -> PathCommand
    where
        T: Transformation,
    {
        match self {
            PathCommand::MoveTo(p) => PathCommand::MoveTo(p.transform(t)),
            PathCommand::LineTo(p) => PathCommand::LineTo(p.transform(t)),
            PathCommand::QuadraticTo(p1, p) => {
                PathCommand::QuadraticTo(p1.transform(t), p.transform(t))
            }
            PathCommand::CubicTo(p1, p2, p) => {
                PathCommand::CubicTo(p1.transform(t), p2.transform(t), p.transform(t))
            }
            PathCommand::Close => PathCommand::Close,
        }
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}

/// An extension trait for iterators over line path commands.
pub trait LinePathIterator: InternalIterator<Item = LinePathCommand> {}

impl<I> LinePathIterator for I where I: InternalIterator<Item = LinePathCommand> {}

/// A command in a line path
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LinePathCommand {
    MoveTo(Point),
    LineTo(Point),
    Close,
}

impl Transform for LinePathCommand {
    fn transform<T>(self, t: &T) -> LinePathCommand
    where
        T: Transformation,
    {
        match self {
            LinePathCommand::MoveTo(p) => LinePathCommand::MoveTo(p.transform(t)),
            LinePathCommand::LineTo(p) => LinePathCommand::LineTo(p.transform(t)),
            LinePathCommand::Close => LinePathCommand::Close,
        }
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}

pub mod quadratic {
    use crate::iter::InternalIterator;

    use super::{Point, Transform, Transformation};

    /// A quadratic bezier curve segment in 2-dimensional Euclidian space.
    #[derive(Clone, Copy, Debug, PartialEq)]
    #[repr(C)]
    pub struct QuadraticSegment {
        pub p0: Point,
        pub p1: Point,
        pub p2: Point,
    }

    impl QuadraticSegment {
        /// Creates a new quadratic bezier curve segment with the given control points.
        pub fn new(p0: Point, p1: Point, p2: Point) -> QuadraticSegment {
            QuadraticSegment { p0, p1, p2 }
        }

        /// Returns true if `self` is approximately linear with tolerance `epsilon`.
        pub fn is_approximately_linear(self, epsilon: f32) -> bool {
            let v1 = self.p1 - self.p0;
            if let Some(vx) = (self.p2 - self.p0).normalize() {
                // If the baseline is a line segment, the segment is approximately linear if the
                // rejection of the control point from the baseline is less than `epsilon`.
                v1.cross(vx).abs() < epsilon
            } else {
                // If the baseline is a single point, the segment is approximately linear if the
                // distance of the control point from the baseline is less than `epsilon`.
                v1.length() < epsilon
            }
        }

        /// Splits `self` into two quadratic Bezier curve segments, at parameter `t`.
        pub fn split(self, t: f32) -> (QuadraticSegment, QuadraticSegment) {
            let p01 = self.p0.lerp(self.p1, t);
            let p12 = self.p1.lerp(self.p2, t);
            let p012 = p01.lerp(p12, t);
            (
                QuadraticSegment::new(self.p0, p01, p012),
                QuadraticSegment::new(p012, p12, self.p2),
            )
        }

        /// Returns an iterator over the points of a polyline that approximates `self` with tolerance
        /// `epsilon`, *excluding* the first point.
        pub fn linearize(self, epsilon: f32) -> Linearize {
            Linearize {
                segment: self,
                epsilon,
            }
        }
    }

    impl Transform for QuadraticSegment {
        fn transform<T>(self, t: &T) -> QuadraticSegment
        where
            T: Transformation,
        {
            QuadraticSegment::new(
                self.p0.transform(t),
                self.p1.transform(t),
                self.p2.transform(t),
            )
        }

        fn transform_mut<T>(&mut self, t: &T)
        where
            T: Transformation,
        {
            *self = self.transform(t);
        }
    }

    /// An iterator over the points of a polyline that approximates `self` with tolerance `epsilon`,
    /// *excluding* the first point.
    #[derive(Clone, Copy)]
    pub struct Linearize {
        segment: QuadraticSegment,
        epsilon: f32,
    }

    impl InternalIterator for Linearize {
        type Item = Point;

        fn for_each(self, f: &mut impl FnMut(Self::Item) -> bool) -> bool {
            if self.segment.is_approximately_linear(self.epsilon) {
                return f(self.segment.p2);
            }
            let (segment_0, segment_1) = self.segment.split(0.5);
            if !segment_0.linearize(self.epsilon).for_each(f) {
                return false;
            }
            segment_1.linearize(self.epsilon).for_each(f)
        }
    }
}
pub mod cubic {
    use crate::iter::InternalIterator;

    use super::{Point, Transform, Transformation};

    /// A cubic bezier curve segment in 2-dimensional Euclidian space.
    #[derive(Clone, Copy, Debug, PartialEq)]
    #[repr(C)]
    pub struct CubicSegment {
        pub p0: Point,
        pub p1: Point,
        pub p2: Point,
        pub p3: Point,
    }

    impl CubicSegment {
        /// Creates a new cubic bezier curve segment with the given control points.
        pub fn new(p0: Point, p1: Point, p2: Point, p3: Point) -> CubicSegment {
            CubicSegment { p0, p1, p2, p3 }
        }

        /// Returns true if `self` is approximately linear with tolerance `epsilon`.
        pub fn is_approximately_linear(self, epsilon: f32) -> bool {
            let v1 = self.p1 - self.p0;
            let v2 = self.p2 - self.p0;
            if let Some(vx) = (self.p3 - self.p0).normalize() {
                // If the baseline is a line segment, the segment is approximately linear if the
                // rejection of both control points from the baseline is less than `epsilon`.
                v1.cross(vx).abs() < epsilon && v2.cross(vx).abs() < epsilon
            } else {
                // If the baseline is a single point, the segment is approximately linear if the
                // distance of both control points from the baseline is less than `epsilon`.
                v1.length() < epsilon && v2.length() < epsilon
            }
        }

        /// Splits `self` into two quadratic Bezier curve segments, at parameter `t`.
        pub fn split(self, t: f32) -> (CubicSegment, CubicSegment) {
            let p01 = self.p0.lerp(self.p1, t);
            let p12 = self.p1.lerp(self.p2, t);
            let p23 = self.p2.lerp(self.p3, t);
            let p012 = p01.lerp(p12, t);
            let p123 = p12.lerp(p23, t);
            let p0123 = p012.lerp(p123, t);
            (
                CubicSegment::new(self.p0, p01, p012, p0123),
                CubicSegment::new(p0123, p123, p23, self.p3),
            )
        }

        /// Returns an iterator over the points of a polyline that approximates `self` with tolerance
        /// `epsilon`, *excluding* the first point.
        pub fn linearize(self, epsilon: f32) -> Linearize {
            Linearize {
                segment: self,
                epsilon,
            }
        }
    }

    impl Transform for CubicSegment {
        fn transform<T>(self, t: &T) -> CubicSegment
        where
            T: Transformation,
        {
            CubicSegment::new(
                self.p0.transform(t),
                self.p1.transform(t),
                self.p2.transform(t),
                self.p3.transform(t),
            )
        }

        fn transform_mut<T>(&mut self, t: &T)
        where
            T: Transformation,
        {
            *self = self.transform(t);
        }
    }
    /// An iterator over the points of a polyline that approximates `self` with tolerance `epsilon`,
    /// *excluding* the first point.
    #[derive(Clone, Copy)]
    pub struct Linearize {
        segment: CubicSegment,
        epsilon: f32,
    }

    impl InternalIterator for Linearize {
        type Item = Point;

        fn for_each(self, f: &mut impl FnMut(Point) -> bool) -> bool {
            if self.segment.is_approximately_linear(self.epsilon) {
                return f(self.segment.p3);
            }
            let (segment_0, segment_1) = self.segment.split(0.5);
            if !segment_0.linearize(self.epsilon).for_each(f) {
                return false;
            }
            segment_1.linearize(self.epsilon).for_each(f)
        }
    }
}
