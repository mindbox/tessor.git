pub trait FromInternalIterator<T> {
	fn from_internal_iter<I: IntoInternalIterator<Item = T>>(iter: I) -> Self;
}

impl<T> FromInternalIterator<T> for Vec<T> {
	fn from_internal_iter<I: IntoInternalIterator<Item = T>>(iter: I) -> Self {
		let mut vec = Vec::new();
		iter.into_internal_iter().for_each(&mut |item| {
			vec.push(item);
			true
		});
		vec
	}
}
pub trait InternalIterator {
	type Item;

	fn for_each(self, f: &mut impl FnMut(Self::Item) -> bool) -> bool;

	fn collect<F: FromInternalIterator<Self::Item>>(self) -> F
	where
		Self: Sized,
	{
		FromInternalIterator::from_internal_iter(self)
	}
}

impl<I: Iterator> InternalIterator for I {
	type Item = I::Item;

	fn for_each(self, f: &mut impl FnMut(Self::Item) -> bool) -> bool {
		for item in self {
			if !f(item) {
				return false;
			}
		}
		true
	}
}
pub trait IntoInternalIterator {
	type Item;

	type IntoInternalIter: InternalIterator<Item = Self::Item>;

	fn into_internal_iter(self) -> Self::IntoInternalIter;
}

impl<I: InternalIterator> IntoInternalIterator for I {
	type Item = <Self as InternalIterator>::Item;

	type IntoInternalIter = Self;

	fn into_internal_iter(self) -> Self::IntoInternalIter {
		self
	}
}

pub trait ExtendFromInternalIterator<T> {
	/// Extends `self` with each item of `internal_iter`.
	fn extend_from_internal_iter<I>(&mut self, internal_iter: I)
	where
		I: IntoInternalIterator<Item = T>;
}

impl<T> ExtendFromInternalIterator<T> for Vec<T> {
	fn extend_from_internal_iter<I>(&mut self, internal_iter: I)
	where
		I: IntoInternalIterator<Item = T>,
	{
		internal_iter.into_internal_iter().for_each(&mut |item| {
			self.push(item);
			true
		});
	}
}
