pub use crate::clipper::FillRule;

use crate::clipper;
use crate::clipper::{Clipper, Operation};
use crate::geom::polygon::Polygon;
use crate::mesh::{Mesh, Writer};
use crate::tessellator;
use crate::tessellator::{ActiveEdge, Tessellator};

#[derive(Clone, Debug)]
pub struct Filler {
	clipper: Clipper,
	clipper_pending_edges: Vec<clipper::PendingEdge>,
	clipper_left_edges: Vec<usize>,
	clipper_right_edges: Vec<usize>,
	clipper_left_boundary_edge_indices: Vec<usize>,
	clipper_right_boundary_edge_indices: Vec<usize>,
	tessellator: Tessellator,
	tessellator_pending_edges: Vec<tessellator::PendingEdge>,
	tessellator_left_edges: Vec<ActiveEdge>,
}

impl Filler {
	pub fn new() -> Self {
		Self {
			clipper: Clipper::new(),
			clipper_pending_edges: Vec::new(),
			clipper_left_edges: Vec::new(),
			clipper_right_edges: Vec::new(),
			clipper_left_boundary_edge_indices: Vec::new(),
			clipper_right_boundary_edge_indices: Vec::new(),
			tessellator: Tessellator::new(),
			tessellator_pending_edges: Vec::new(),
			tessellator_left_edges: Vec::new(),
		}
	}
	pub fn fill(&mut self, polygons: &[Polygon], fill_rule: FillRule, output_mesh: &mut Mesh) {
		self.tessellator.tessellate(
			self.clipper.clip_polygons(
				Operation::Union,
				polygons,
				&[],
				clipper::Options {
					subject_fill_rule: fill_rule,
					..clipper::Options::default()
				},
				&mut self.clipper_pending_edges,
				&mut self.clipper_left_edges,
				&mut self.clipper_right_edges,
				&mut self.clipper_left_boundary_edge_indices,
				&mut self.clipper_right_boundary_edge_indices,
			),
			&mut Writer::new(output_mesh),
			&mut self.tessellator_pending_edges,
			&mut self.tessellator_left_edges,
		);
	}
}
