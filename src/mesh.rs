pub trait Callbacks {
	fn vertex(&mut self, x: f32, y: f32) -> u16;

	fn triangle(&mut self, index_0: u16, index_1: u16, index_2: u16);
}

#[derive(Clone, Debug, Default, PartialEq)]
#[repr(C)]
pub struct Mesh {
	pub vertices: Vec<[f32; 2]>,
	pub indices: Vec<u32>,
}

impl Mesh {
	pub fn new() -> Self {
		Self::default()
	}
}

#[derive(Debug)]
pub struct Writer<'a> {
	pub vertices: &'a mut Vec<[f32; 2]>,
	pub indices: &'a mut Vec<u32>,
}

impl<'a> Writer<'a> {
	pub fn new(mesh: &'a mut Mesh) -> Self {
		Self {
			vertices: &mut mesh.vertices,
			indices: &mut mesh.indices,
		}
	}
}

impl<'a> Callbacks for Writer<'a> {
	fn vertex(&mut self, x: f32, y: f32) -> u16 {
		let vertex = [x, y];
		let index = self.vertices.len() as u16;
		self.vertices.push(vertex);
		index
	}

	fn triangle(&mut self, index_0: u16, index_1: u16, index_2: u16) {
		self.indices.extend(&[index_0 as _, index_1 as _, index_2 as _]);
	}
}
