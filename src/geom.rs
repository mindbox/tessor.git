use std::cmp::Ordering;
use std::ops::{Add, Div, Mul, Neg, Sub};

#[derive(Clone, Copy, Debug)]
pub struct Vector {
	x: f32,
	y: f32,
}
impl Vector {
	pub fn new(x: f32, y: f32) -> Self {
		Self { x, y }
	}
	pub fn x(self) -> f32 {
		self.x
	}
	pub fn y(self) -> f32 {
		self.y
	}
	pub fn length(self) -> f32 {
		self.x.hypot(self.y)
	}
	pub fn dot(self, other: Self) -> f32 {
		self.x * other.x + self.y * other.y
	}
	pub fn cross(self, other: Self) -> f32 {
		self.x * other.y - other.x * self.y
	}
	pub fn normalize(self) -> Option<Self> {
		let length = self.length();
		if length == 0.0 {
			None
		} else {
			Some(self / length)
		}
	}
	pub fn perpendicular(self) -> Self {
		Self { x: -self.y, y: self.x }
	}
}
impl Add for Vector {
	type Output = Self;

	fn add(self, other: Self) -> Self::Output {
		Self {
			x: self.x + other.x,
			y: self.y + other.y,
		}
	}
}
impl Sub for Vector {
	type Output = Self;

	fn sub(self, other: Self) -> Self::Output {
		Self {
			x: self.x - other.x,
			y: self.y - other.y,
		}
	}
}
impl Mul<f32> for Vector {
	type Output = Self;

	fn mul(self, k: f32) -> Self::Output {
		Self {
			x: self.x * k,
			y: self.y * k,
		}
	}
}
impl Div<f32> for Vector {
	type Output = Self;

	fn div(self, k: f32) -> Self::Output {
		Self {
			x: self.x / k,
			y: self.y / k,
		}
	}
}
impl Neg for Vector {
	type Output = Self;

	fn neg(self) -> Self::Output {
		Self { x: -self.x, y: -self.y }
	}
}

#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub struct Point {
	x: f32,
	y: f32,
}
impl Point {
	pub fn new(x: f32, y: f32) -> Self {
		Self { x, y }
	}
	pub fn x(self) -> f32 {
		self.x
	}
	pub fn y(self) -> f32 {
		self.y
	}
	pub fn min(self, other: Self) -> Self {
		if self <= other {
			self
		} else {
			other
		}
	}
	pub fn max(self, other: Self) -> Self {
		if self <= other {
			other
		} else {
			self
		}
	}
	pub fn lerp(self, other: Self, t: f32) -> Self {
		Self {
			x: self.x * (1.0 - t) + other.x * t,
			y: self.y * (1.0 - t) + other.y * t,
		}
	}
}
impl Add<Vector> for Point {
	type Output = Self;
	fn add(self, v: Vector) -> Self::Output {
		Self {
			x: self.x + v.x(),
			y: self.y + v.y(),
		}
	}
}
impl Sub for Point {
	type Output = Vector;
	fn sub(self, other: Self) -> Self::Output {
		Vector::new(self.x() - other.x(), self.y() - other.y())
	}
}
impl Sub<Vector> for Point {
	type Output = Self;
	fn sub(self, v: Vector) -> Self::Output {
		Self {
			x: self.x - v.x(),
			y: self.y - v.y(),
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub struct Rectangle {
	start: Point,
	end: Point,
}
impl Rectangle {
	pub fn new(start: Point, end: Point) -> Self {
		Self { start, end }
	}
	pub fn start(self) -> Point {
		self.start
	}
	pub fn end(self) -> Point {
		self.end
	}
	pub fn clamp(self, point: Point) -> Point {
		Point::new(
			point.x().max(self.start.x()).min(self.end.x()),
			point.y().max(self.start.y()).min(self.end.y()),
		)
	}
	pub fn intersect(self, other: Self) -> Self {
		Self {
			start: Point::new(self.start.x().max(other.start.x()), self.start.y().max(other.start.y())),
			end: Point::new(self.end.x().min(other.end.x()), self.end.y().min(other.end.y())),
		}
	}
}

#[derive(Clone, Copy, Debug)]
pub struct LineSegment {
	start: Point,
	end: Point,
}
impl LineSegment {
	pub fn new(start: Point, end: Point) -> Self {
		Self { start, end }
	}
	pub fn start(self) -> Point {
		self.start
	}
	pub fn end(self) -> Point {
		self.end
	}
	pub fn compare_to_point(self, point: Point) -> Option<Ordering> {
		(point - self.start).cross(self.end - point).partial_cmp(&0.0)
	}
	pub fn intersect(self, other: Self) -> Option<Point> {
		let a = self.end - self.start;
		let b = other.start - other.end;
		let c = self.start - other.start;
		let denom = a.cross(b);
		if denom == 0.0 {
			return None;
		}
		let numer_0 = b.cross(c);
		let numer_1 = c.cross(a);
		if denom < 0.0 {
			if (numer_0 < denom || 0.0 < numer_0) || (numer_1 < denom || 0.0 < numer_1) {
				return None;
			}
		} else {
			if (numer_0 < 0.0 || denom < numer_0) || (numer_1 < 0.0 || denom < numer_1) {
				return None;
			}
		}
		Some(self.start.lerp(self.end, numer_0 / denom))
	}
	pub fn normal(self) -> Vector {
		self.tangent().perpendicular()
	}
	pub fn tangent(self) -> Vector {
		self.end - self.start
	}
	pub fn bounds(self) -> Rectangle {
		Rectangle::new(
			Point::new(self.start.x().min(self.end.x()), self.start.y().min(self.end.y())),
			Point::new(self.start.x().max(self.end.x()), self.start.y().max(self.end.y())),
		)
	}
	pub fn reverse(self) -> Self {
		Self {
			end: self.start,
			start: self.end,
		}
	}
}

pub use polyline::Polyline;
pub mod polyline {
	use super::{LineSegment, Point};
	use std::slice::Iter;

	#[derive(Clone, Debug, PartialEq)]
	pub struct Polyline {
		pub vertices: Vec<Point>,
	}
	impl Polyline {
		pub fn edges(&self) -> Edges {
			Edges {
				first_vertex: self.vertices.first(),
				vertices_iter: self.vertices[1..self.vertices.len() - 1].iter(),
				last_vertex: self.vertices.last(),
			}
		}
	}
	#[derive(Clone, Debug)]
	pub struct Edges<'a> {
		first_vertex: Option<&'a Point>,
		vertices_iter: Iter<'a, Point>,
		last_vertex: Option<&'a Point>,
	}
	impl<'a> Iterator for Edges<'a> {
		type Item = LineSegment;

		fn next(&mut self) -> Option<Self::Item> {
			if let Some(vertex) = self.vertices_iter.next() {
				let first_vertex = self.first_vertex.replace(vertex).unwrap();
				return Some(LineSegment::new(*first_vertex, *vertex));
			}
			if let Some(last_vertex) = self.last_vertex.take() {
				let first_vertex = self.first_vertex.take().unwrap();
				return Some(LineSegment::new(*first_vertex, *last_vertex));
			}
			None
		}
	}
	impl<'a> DoubleEndedIterator for Edges<'a> {
		fn next_back(&mut self) -> Option<Self::Item> {
			if let Some(vertex) = self.vertices_iter.next_back() {
				let last_vertex = self.last_vertex.replace(vertex).unwrap();
				return Some(LineSegment::new(*vertex, *last_vertex));
			}
			if let Some(first_vertex) = self.first_vertex.take() {
				let last_vertex = self.last_vertex.take().unwrap();
				return Some(LineSegment::new(*first_vertex, *last_vertex));
			}
			None
		}
	}
}

pub use polygon::Polygon;
pub mod polygon {
	use super::{LineSegment, Point};
	use std::slice::Iter;

	#[derive(Clone, Debug, PartialEq)]
	pub struct Polygon {
		pub vertices: Vec<Point>,
	}
	impl Polygon {
		pub fn edges(&self) -> Edges {
			Edges {
				first_vertex: self.vertices.first(),
				vertex_iter: self.vertices[1..].iter(),
				last_vertex: self.vertices.first(),
			}
		}
	}
	#[derive(Clone, Debug)]
	pub struct Edges<'a> {
		first_vertex: Option<&'a Point>,
		vertex_iter: Iter<'a, Point>,
		last_vertex: Option<&'a Point>,
	}
	impl<'a> Iterator for Edges<'a> {
		type Item = LineSegment;

		fn next(&mut self) -> Option<Self::Item> {
			if let Some(vertex) = self.vertex_iter.next() {
				let first_vertex = self.first_vertex.replace(vertex).unwrap();
				return Some(LineSegment::new(*first_vertex, *vertex));
			}
			if let Some(vertex) = self.last_vertex.take() {
				let first_vertex = self.first_vertex.take().unwrap();
				return Some(LineSegment::new(*first_vertex, *vertex));
			}
			None
		}
	}
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Command {
	MoveTo(Point),
	Close,
	LineTo(Point),
}
impl Polygon {
	pub fn commands(&self) -> Vec<Command> {
		let mut commands = Vec::new();
		commands.push(Command::MoveTo(self.vertices.first().cloned().unwrap()));
		for vertex in self.vertices[1..].iter().cloned() {
			commands.push(Command::LineTo(vertex));
		}
		commands.push(Command::Close);
		commands
	}
}



/// A trait to transform geometric objects in 2-dimensional Euclidian space.
pub trait Transform {
    fn transform<T>(self, t: &T) -> Self
    where
        T: Transformation;

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation;
}
impl Transform for Vector {
    fn transform<T>(self, t: &T) -> Vector
    where
        T: Transformation,
    {
        t.transform_vector(self)
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}
impl Transform for Point {
    fn transform<T>(self, t: &T) -> Point
    where
        T: Transformation,
    {
        t.transform_point(self)
    }

    fn transform_mut<T>(&mut self, t: &T)
    where
        T: Transformation,
    {
        *self = self.transform(t);
    }
}

/// A trait for transformations in 2-dimensional Euclidian space.
pub trait Transformation {
    /// Applies `self` to the given `point`.
    fn transform_point(&self, point: Point) -> Point;

    /// Applies `self` to the given `vector`.
    fn transform_vector(&self, vector: Vector) -> Vector;
}
